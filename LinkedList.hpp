/**
 * Tutorial 2 - Linked List
 * @author Faiz Shukri
 */

#include <iostream>
using namespace std;

template <typename T>
struct Node	{
	T data;
	Node<T>* next;
};

template <typename T>
class LinkedList {
	Node<T>* head;
	int count;

public:
	LinkedList();
	~LinkedList();
	
	bool isEmpty() {return head == NULL;}
	int size() {return count;};

	bool insertNode(int index, T);

	int findNode(T data);
	T getNode(int index); // Same as findNode, but using index instead of data

	int deleteNode(T data);
	T removeNode(int index); // Same as deleteNode, but using index instead of data

	void printList();
};
