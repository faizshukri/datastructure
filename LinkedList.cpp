/**
 * Tutorial 2 - Linked List
 * @author Faiz Shukri
 */

#include "LinkedList.hpp"

template <typename T>
LinkedList<T>::LinkedList()
{
}

template <typename T>
LinkedList<T>::~LinkedList()
{
}

template <typename T>
bool LinkedList<T>::insertNode(int index /* start with 0 */, T data)
{
}

template <typename T>
int LinkedList<T>::findNode(T data)
{
}

template <typename T>
T LinkedList<T>::getNode(int index)
{
}

template <typename T>
int LinkedList<T>::deleteNode(T data)
{
}

template <typename T>
T LinkedList<T>::removeNode(int index)
{
}

template <typename T>
void LinkedList<T>::printList()
{
}

template class LinkedList<int>;
template class LinkedList<char>;
