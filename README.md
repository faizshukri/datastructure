CSC Data Structure and Algoritm
===============================

## Tutorial 2 - Linked List

Implement `LinkedList.cpp` and make sure `tutorial_2_linkedlist.cpp` can be run and get the correct output.

To compile, run this command

```bash
g++ tutorial_2_linkedlist.cpp LinkedList.cpp -o tutorial_2.exe
```

Run the program by simple execute the binary `tutorial_2.exe`
