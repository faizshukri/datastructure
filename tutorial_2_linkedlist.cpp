#include <iostream>
#include "LinkedList.hpp"
using namespace std;

int main() {
	LinkedList<int> list;

	cout << "Initial list\n";
	list.printList(); // []

	// Insert few item into list
	list.insertNode(0, 10); // [10]
	list.insertNode(0, 12); // [12, 10]
	list.insertNode(2, 15); // [12, 10, 15]
	list.insertNode(2, 20); // [12, 10, 20, 15]

	cout << "After insertion\n";
	list.printList(); // [12, 10, 20, 15]

	// Find item 10
	int position10 = list.findNode(10);
	if(position10 > 0) {
		cout << "Item 10 found at position: " << position10 << endl;
	} else {
		cout << "Item 10 is not found\n";
	}

	// Find item 13
	int position13 = list.findNode(13);
	if(position13 > 0) {
		cout << "Item 13 found at position: " << position13 << endl;
	} else {
		cout << "Item 13 is not found\n";
	}

	// Delete node
	list.deleteNode(10); // [12, 20, 15]
	list.deleteNode(15); // [12, 20]

	cout << "\nAfter deletion\n";
	list.printList(); // [12, 20]

	return 0;
}
